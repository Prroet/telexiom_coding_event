# Use node stable because we can
FROM node

WORKDIR /usr/src/app

RUN npm install

# Add everything under App
ADD App/ ./

CMD ["npm", "start"]
