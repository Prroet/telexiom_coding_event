"use strict";

// server stuff
var express = require('express');
var app = express();

var port_number=3000;

//logging
var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'debug';
// for printing objects to console
const util = require('util');

// mongo db stuff
var MongoClient = require('mongodb').MongoClient;

// for reading files
var fs = require('fs');

var url = "mongodb://mongo:27017/coding_event";

var glbl_db=null;

var bodyParser = require('body-parser');

MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    glbl_db=db; // global db connection for all functions
    logger.debug("Database created!");
    import_data(glbl_db, "customers.json");
    import_data(glbl_db, "products.json");
});

//TODO add routing
// call to homepage
app.get('/', function(req, res){
  res.send('Hi!');
});

// lists all customers
app.get('/list/customers', function(req, res) {
  let customer_db = glbl_db.db('coding_event');
  customer_db.collection("customers").find({}).toArray(function(err, result){
    if(err) throw err;
    logger.debug("All customers from db: " + util.inspect(result));
                        // pre for pretty view in browser
    let all_customers = JSON.stringify(result,null, 2) // for pretty view in browser
    res.send(all_customers);
  });
});

// lists specific customer
// params in get request are: firstname and lastname
//TODO require json to be send. Do not read parameter!
app.get('/list/customer/', function(req, res){
  let customer_db = glbl_db.db('coding_event');
  let customer=[req.query.firstname, req.query.lastname];
  customer_db.collection("customers").find({"firstname":customer[0], "lastname":customer[1]}).toArray(function(err,item){
    if(err) throw err;
    logger.debug("Customer: " + util.inspect(item));
    res.send(JSON.stringify(item, null, 2)); 
  });
});

// lists all products
app.get('/list/products', function(req, res){
  let customer_db = glbl_db.db('coding_event');
  let customer=[req.query.firstname, req.query.lastname];
  customer_db.collection("products").find({}).toArray(function(err,products){
    if(err) throw err;
    logger.debug("Products: " + util.inspect(products));
    res.send(JSON.stringify(products, null, 2)); 
  });
});

// returns the customer from database by id
function get_customer_by_id(id){
  let customer_db=glbl_db.db('coding_event');
  customer_db.collection("customers").find({_id:id}).toArray(function(err,product){
    
  });
}

/** bodyParser.urlencoded(options)
 * Parses the text as URL encoded data (which is how browsers tend to send form data from regular forms set to POST)
 * and exposes the resulting object (containing the keys and values) on req.body
 */
app.use(bodyParser.urlencoded({
    extended: true
}));

/**bodyParser.json(options)
 * Parses the text as JSON and exposes the resulting object on req.body.
 */
app.use(bodyParser.json());

//this will place an order object into db
// 
app.post('/place/order', function(req,res){
  let order = req.body;
  console.log("Order: " + JSON.stringify(order));
  order.placed=Date.now();
  let coding_event_db = glbl_db.db('coding_event');
  let collection_name = "orders";
  let collection_data=order;
  coding_event_db.collection(collection_name).insert(collection_data, function(err, res){
    if (err) throw err;
    logger.debug("inserted: " + res);
  });
  res.send(order);
});

app.post('/insert/customer', function(req,res){
  let customer = req.body;
  console.log("Customer: " + JSON.stringify(customer));
  let coding_event_db = glbl_db.db('coding_event');
  let collection_name = "customers";
  let collection_data=customer;
  coding_event_db.collection(collection_name).insert(collection_data, function(err, res){
    if (err) throw err;
    logger.debug("inserted: " + res);
  });
  res.send(customer);
});

app.get('/list/orders', function(req,res){
  let coding_event_db=glbl_db.db('coding_event');
  coding_event_db.collection('orders').find().toArray(function(err,orders){
    if (err) throw err;
    logger.debug("Sending all orders + " + JSON.stringify(orders));
    res.send(orders);
  });
});

/**
  Server main listen function
**/
app.listen(port_number, function(){
	logger.debug('listening on ' + port_number);  
});

/**
* imports data from json into mongodb
* params: database to connect to and the import file.
* import file needs to be json!!!
*/
function import_data(database, import_file){
  let coding_event_db = database.db('coding_event');
  fs.readFile(import_file, function(err, data ){
    if(err) throw err;
    let collection_data = JSON.parse(data);
    let collection_name = import_file.slice(0,-5); // remove .json file extension
    logger.debug(collection_name+" "+ util.inspect(collection_data, false, null));
    logger.debug("db object" + util.inspect(coding_event_db));
    coding_event_db.listCollections({name:collection_name}).toArray(function(err, collections){
      logger.debug("All collections names" + util.inspect(collections));
      if(collections.length > 0) {
        logger.debug("Collection " + collection_name + " already exists!");
      }
      else{ // if no collection exists, then create it!
        coding_event_db.createCollection(collection_name, function(err, res) {
          if (err) throw err;
          logger.debug("Collection "+collection_name +" created!");
          coding_event_db.collection(collection_name).insert(collection_data, function(err, res){
            if (err) throw err;
            logger.debug("inserted: " + res);
          });
        });
      }
    }); 
  }); 
}
