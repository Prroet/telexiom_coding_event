# Coding event

This is the server for the coding event from telexiom at the 
h_da

# Server
The server only writes data into database and reads from database and writes
the data to the http endpoints.
If the Port 3000 is free then the server is available via Port 3000. If not
the docker-compose.yml file needs to be modified

# GET Methods
The Get Methods all return json wrapped in &lt;pre&gt;&lt;/pre&gt; html tags
* http://localhost:3000/  -> returns hi to the client
* http://localhost:3000/list/customers  -> lists all existing customers in the database
* http://localhost:3000/list/products   -> lists all products in the database
* http://localhost:3000/list/orders     -> lists all orders in database

# POST Method
The POST Method wants json in the following format:
```javascript
{
  "customer_id":"the id of the customer", 
  "product_id":"the id from the product", 
  "placed":"UTC-Timestamp in milliseconds"
}
```
Path to the POST Method:

* http://localhost:3000/place/order

